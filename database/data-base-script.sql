CREATE DATABASE DEV_StudySchedule;

GO
USE [DEV_StudySchedule]
GO

CREATE TABLE desired_study(
	"id" INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY,
	"user_uid" VARCHAR(128) NOT NULL
);
GO

CREATE TABLE desired_course(
	"id" INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY,
	"desired_study_id" INTEGER NOT NULL,
	"name" VARCHAR(100) NOT NULL,
	"required" VARCHAR(100) NOT NULL,
	FOREIGN KEY ("desired_study_id") REFERENCES desired_study("id")
);
GO

CREATE TABLE scheduled_study_option(
	"id" INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY,
	"desired_study_id" INTEGER NOT NULL,
	FOREIGN KEY ("desired_study_id") REFERENCES desired_study("id")
);
GO

CREATE TABLE scheduled_course(
	"id" INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY,
	"scheduled_study_option_id" INTEGER NOT NULL,
	"name" VARCHAR(100) NOT NULL,
	"order" INTEGER NOT NULL,
	FOREIGN KEY ("scheduled_study_option_id") REFERENCES scheduled_study_option("id")
);
GO