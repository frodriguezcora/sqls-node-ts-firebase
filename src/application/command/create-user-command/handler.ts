import { auth } from "firebase-admin";
import { CreateUserCommand } from "./command";
import { ValidationError } from "@application/validation-error";

const createUserCommandHandler = async (command: CreateUserCommand) => {
    if (!command.email || !command.password) {
        throw new ValidationError("The email and password are required values.");
    }

    const { uid } = await auth().createUser({ 
        email: command.email,
        password: command.password
    });
}

export {
    createUserCommandHandler
}