import { SignInUserCommand } from "./command";
import { ValidationError } from "@application/validation-error";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { SignInUserCommandDto } from "./dto";

const signInUserCommandHandler = async (command: SignInUserCommand): Promise<SignInUserCommandDto> => {
    if (!command.email || !command.password) {
        throw new ValidationError("The email and password are required values.");
    }
    
    const auth = getAuth();
    const sigInResponse = await signInWithEmailAndPassword(auth, command.email, command.password);
    const token = await sigInResponse.user.getIdToken();

    return new SignInUserCommandDto(
        command.email,
        command.email,
        sigInResponse.user.emailVerified,
        token);
}

export {
    signInUserCommandHandler
}