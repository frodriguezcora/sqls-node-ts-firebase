class SignInUserCommandDto {
    email: string;
    name: string;
    verified: boolean;
    token: string;

    constructor(email: string, name: string, verified: boolean, token: string) {
        this.email = email;
        this.name = name;
        this.verified = verified;
        this.token = token;
    }
}

export {
    SignInUserCommandDto,
}