import { TokenInformation } from "@api/helper";

class CreateStudyDesiredCommand {
    context: TokenInformation;
    selectedCourses: CourseDesired[];

    constructor(context: TokenInformation, selectedCourses: CourseDesired[]) {
        this.context = context;
        this.selectedCourses = selectedCourses;
    }
}

class CourseDesired {
    desiredCourse: string;
    requiredCourse: string;

    constructor(desiredCourse: string, requiredCourse: string) {
        this.desiredCourse = desiredCourse;
        this.requiredCourse = requiredCourse;
    }
}

export {
    CreateStudyDesiredCommand,
    CourseDesired
}