import { StudyDesiredId } from "@domain/study-desired";

class CreateStudyDesiredCommandDto {
    id: number;
    options: StudyScheduleOption[];

    constructor(id: StudyDesiredId, options: StudyScheduleOption[]) {
        this.id = id.value;
        this.options = options;
    }
}

class StudyScheduleOption {
    id: number;
    courses: ScheduledCourseOption[];

    constructor(id: number, courses: ScheduledCourseOption[]) {
        this.id = id;
        this.courses = courses;
    }
}

class ScheduledCourseOption {
    name: string;
    order: number;

    constructor(name: string, order: number) {
        this.name = name;
        this.order = order;
    }
}

export {
    CreateStudyDesiredCommandDto,
    StudyScheduleOption,
    ScheduledCourseOption
}