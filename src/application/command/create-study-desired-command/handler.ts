import { Student } from "@domain/student";
import { CourseDesired, StudyDesired } from "@domain/study-desired";
import { registStudyScheduleRequest } from "@domain/study-desired/service/regist-study-desired";
import { CreateStudyDesiredCommand } from "./command";
import { CreateStudyDesiredCommandDto, StudyScheduleOption, ScheduledCourseOption } from "./dto";
import { buildStudyScheduleOptions } from "@domain/study-desired/service/build-study-schedule-options";
import { registStudySchedule } from "@domain/study-schedule/service/regist-study-schedule";
import { ValidationError } from "@application/validation-error";

const createStudyDesiredCommandHandler = async (command: CreateStudyDesiredCommand): Promise<CreateStudyDesiredCommandDto> => {
    const student = new Student(
        command.context.userId,
        command.context.email);

    const courses = command.selectedCourses.map(selected => new CourseDesired(
        selected.desiredCourse,
        selected.requiredCourse));

    const studyDesired = new StudyDesired(student, courses);
    await registStudyScheduleRequest(studyDesired);
    
    if (!studyDesired.id) {
        throw new ValidationError("An error occurred in options.");
    }

    const options = buildStudyScheduleOptions(studyDesired.id, studyDesired.courses);
            
    for (const option of options) {
        await registStudySchedule(option);
    }

    const optionDtos = options.map(option => {
        const courses = option.courses.map(course => new ScheduledCourseOption(
            course.name,
            course.order??0
        ));

        return new StudyScheduleOption(option.id?.value??0, courses);
    });

    return new CreateStudyDesiredCommandDto(
        studyDesired.id,
        optionDtos);
}

export {
    createStudyDesiredCommandHandler
}