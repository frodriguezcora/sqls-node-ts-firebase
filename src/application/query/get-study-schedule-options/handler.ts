import { ValidationError } from "@application/validation-error";
import { StudyDesiredId } from "@domain/study-desired";
import { getByStudyDesired } from "@infrastructure/repository/study-schedule-option";
import { GetStudyScheduleOptionsQueryDto, StudyScheduleOption,ScheduledCourse } from "./dto";
import { GetStudyScheduleOptionsQuery } from "./query";

const getStudyScheduleOptionsQueryHandler = async (query: GetStudyScheduleOptionsQuery): Promise<GetStudyScheduleOptionsQueryDto> => {
    const studyDesiredId = new StudyDesiredId(query.studyDesiredId);
    const options = await getByStudyDesired(studyDesiredId);
    
    const optionDtos = options.map(option => {
        const courseDtos = option.courses
            .map(course => new ScheduledCourse(course.name, course.order))
            .sort((a, b) => a.order - b.order);

        if (!option.id?.value) {
            throw new ValidationError("An error occurred retrieving the options.");
        }

        return new StudyScheduleOption(option.id.value, courseDtos)
    });

    return new GetStudyScheduleOptionsQueryDto(optionDtos);
}

export {
    getStudyScheduleOptionsQueryHandler
}