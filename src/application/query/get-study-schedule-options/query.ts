class GetStudyScheduleOptionsQuery {
    studyDesiredId: number;

    constructor(studyDesiredId: number) {
        this.studyDesiredId = studyDesiredId;
    }
}

export {
    GetStudyScheduleOptionsQuery
}