class GetStudyScheduleOptionsQueryDto {
    options: StudyScheduleOption[];

    constructor(options: StudyScheduleOption[]) {
        this.options = options;
    }
}

class StudyScheduleOption {
    id: number;
    courses: ScheduledCourse[];

    constructor(id: number, courses: ScheduledCourse[]) {
        this.id = id;
        this.courses = courses;
    }
}

class ScheduledCourse {
    name: string;
    order: number;

    constructor(name: string, order: number) {
        this.name = name;
        this.order = order;
    }
}

export {
    GetStudyScheduleOptionsQueryDto,
    StudyScheduleOption,
    ScheduledCourse
}