import { Router, Request, Response } from "express";
import { createStudyScheduleOptions, getStudyScheduleOptions } from "@api/controller/study-desired";

const router = Router();

router.get('/study-desired/options/:id', getStudyScheduleOptions);

router.post('/study-desired', createStudyScheduleOptions);

export default router;