import { Router, Request, Response } from "express";
import { create, signIn } from "@api/controller/user";

const router = Router();
 
router.get('/', (request: Request, response: Response) => {
  response.send('Hello to Study Schedule Service...');
});

router.post('/user', create);
router.post('/user/sign-in', signIn);

export default router;