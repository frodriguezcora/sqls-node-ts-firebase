import PublicRouter from "./public";
import PrivateRouter from "./private";

export {
  PublicRouter,
  PrivateRouter
}