import { Request } from "express";

const tokenIsolator = (request: Request): string => {
    const authHeader = request.headers.authorization;
    return authHeader?.split(' ')[1] ?? "";
}

class TokenInformation {
    userId: string;
    email: string;

    constructor(userId: string, email: string) {
        this.userId = userId;
        this.email = email;
    }
}

const tokenDecryptorService = (token: string): TokenInformation => {
    const data = JSON.parse(atob(token.split('.')[1]));
    const response = new TokenInformation(
        data["user_id"],
        data["email"]
    );

    return response;
}

const permutator = <T>(inputArr: T[]): T[][] => {
    let result: T[][] = [];
  
    const permute = (arr: T[], m: T[] = []) => {
      if (arr.length === 0) {
        result.push(m)
      } else {
        for (let i = 0; i < arr.length; i++) {
          let curr = arr.slice();
          let next: T[] = curr.splice(i, 1);
          permute(curr.slice(), m.concat(next))
       }
     }
   }
  
   permute(inputArr)
  
   return result;
}

export {
    permutator,
    tokenIsolator,
    TokenInformation,
    tokenDecryptorService
}
