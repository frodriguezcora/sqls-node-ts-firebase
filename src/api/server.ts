import express from "express";
import bodyParser from "body-parser";
import { PublicRouter, PrivateRouter } from "@api/router";
import { tokenValidation } from "@api/middleware/token-validation";
import { errorHandler } from "@api/middleware/error-handler";

const api = express();

api.use(bodyParser.urlencoded({ extended: false }));
api.use(bodyParser.json());

api.use(PublicRouter);
api.use(tokenValidation);
api.use(PrivateRouter);

api.use(errorHandler);

export default api;
