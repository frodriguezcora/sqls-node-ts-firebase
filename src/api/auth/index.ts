import { initializeApp as initializeAdminApp, applicationDefault } from "firebase-admin/app";
import { initializeApp as initializeClientApp } from "firebase/app";
import FirebaseAuthClientKeys from "./firebase-client-credentials.json";

const initFirebaseAdmin = () => {
    //applicationDefault use GOOGLE_APPLICATION_CREDENTIALS env var
    initializeAdminApp({
        credential: applicationDefault()
    });
}

const initFirebaseClient = () => {
    initializeClientApp(FirebaseAuthClientKeys);
}

export {
    initFirebaseAdmin,
    initFirebaseClient
}