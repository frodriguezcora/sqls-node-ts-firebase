import { auth } from "firebase-admin";
import { Request, Response, NextFunction } from "express";
import { SecurityError } from "@api/security-error";
import { tokenIsolator } from "@api/helper";

const tokenValidation = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = tokenIsolator(req);
        await auth().verifyIdToken(token);
        
        next();
    }
    catch (err: any)
    {
        next(new SecurityError("Unauthorized.", err));
    }
}

export {
    tokenValidation
}