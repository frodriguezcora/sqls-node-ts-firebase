import { NextFunction, Request, Response } from "express";
import { CreateUserCommand, createUserCommandHandler } from "@application/command/create-user-command";
import { SignInUserCommand, signInUserCommandHandler } from "@application/command/sig-in-user-command";

const create = async (request: Request, response: Response, next: NextFunction) => {
    try {   
        const { email, password } = request.body;
        const command = new CreateUserCommand(email, password);
    
        await createUserCommandHandler(command);
        response.status(201).send();
    }
    catch (err)
    {
        next(err);
    }
};

const signIn = async (request: Request, response: Response, next: NextFunction) => {
    try {   
        const { email, password } = request.body;
        const command = new SignInUserCommand(email, password);
    
        const dto = await signInUserCommandHandler(command);
        response.send(dto);
    }
    catch (err)
    {
        next(err);
    }
};

export {
    create,
    signIn
}