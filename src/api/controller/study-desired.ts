import { Request, Response } from "express";
import { tokenIsolator, tokenDecryptorService } from "@api/helper";
import { GetStudyScheduleOptionsQuery } from "@application/query/get-study-schedule-options/query";
import { getStudyScheduleOptionsQueryHandler } from "@application/query/get-study-schedule-options/handler";
import { CreateStudyDesiredCommand } from "@application/command/create-study-desired-command/command";
import { createStudyDesiredCommandHandler } from "@application/command/create-study-desired-command/handler";

const createStudyScheduleOptions = async (request: Request, response: Response) => {
    const { courses } = request.body;
    const token = tokenIsolator(request);
    const tokenInfo = tokenDecryptorService(token);
    const command = new CreateStudyDesiredCommand(tokenInfo, courses);
    const dto = await createStudyDesiredCommandHandler(command);

    response.send(dto);
}

const getStudyScheduleOptions = async (request: Request, response: Response) => {
    const query = new GetStudyScheduleOptionsQuery(parseInt(request.params.id));
    const dto = await getStudyScheduleOptionsQueryHandler(query);

    response.send(dto);
}


export {
    createStudyScheduleOptions,
    getStudyScheduleOptions
}