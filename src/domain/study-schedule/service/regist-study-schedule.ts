import { StudySchedule} from "@domain/study-schedule";
import { create } from "@infrastructure/repository/study-schedule-option";

const registStudySchedule = async(option: StudySchedule) => {
    await create(option);
}

export {
    registStudySchedule
}