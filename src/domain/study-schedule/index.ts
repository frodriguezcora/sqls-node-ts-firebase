import { StudyDesiredId } from "@domain/study-desired";

class StudySchedule {
    id: StudyScheduleId | null;
    studyDesiredId: StudyDesiredId;
    courses: CourseScheduled[];

    constructor(studyDesiredId: StudyDesiredId, courses: CourseScheduled[], id: StudyScheduleId | null = null) {
        this.id = id;
        this.studyDesiredId = studyDesiredId;
        this.courses = courses;
    }
}

class CourseScheduled {
    name: string;
    order: number;

    constructor(name: string, order: number) {
        this.name = name;
        this.order = order;
    }
}

class StudyScheduleId {
    value: number;

    constructor(value: number) {
        this.value = value;
    }
}

export {
    StudySchedule,
    CourseScheduled,
    StudyScheduleId
}