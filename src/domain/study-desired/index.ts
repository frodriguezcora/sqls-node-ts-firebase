import { Student } from "@domain/student";

class StudyDesired {
    student: Student;
    courses: CourseDesired[];
    id: StudyDesiredId | null;

    constructor(student: Student, courses: CourseDesired[], id: StudyDesiredId | null = null) {
        this.id = id;
        this.student = student;
        this.courses = courses;
    }
}

class CourseDesired {
    name: string;
    dependency: string;

    constructor(name: string, dependency: string) {
        this.name = name;
        this.dependency = dependency;
    }
}

class StudyDesiredId {
    value: number;

    constructor(value: number) {
        this.value = value;
    }
}

export {
    CourseDesired,
    StudyDesired,
    StudyDesiredId
}