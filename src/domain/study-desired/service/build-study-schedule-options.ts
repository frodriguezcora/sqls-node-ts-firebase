import { permutator } from "@api/helper";
import { DomainError } from "@domain/domain-error";
import { CourseDesired, StudyDesiredId } from "@domain/study-desired";
import { StudySchedule, CourseScheduled } from "@domain/study-schedule";

const buildStudyScheduleOptions = (studyDesiredId: StudyDesiredId, selectedCourses: CourseDesired[]): StudySchedule[] => {
    if (selectedCourses.length == 0) {
        throw new DomainError("The selected courses must be not empty.");
    }

    const result: StudySchedule[] = [];

    const requiredCourses = selectedCourses.map((course: CourseDesired) => course.dependency);
    const desiredCourses = selectedCourses.map((course: CourseDesired) =>  course.name);
    const availableCourses = requiredCourses.filter((required: string) => !desiredCourses.includes(required));

    if (availableCourses.length == 0) {
        throw new DomainError("The selected courses must contain at least a starter available course.");
    }

    const allCourses = new Set([...requiredCourses, ...desiredCourses]);
    const options = buildOptions(availableCourses, selectedCourses);

    for (const option of options) {
        if (option.length != allCourses.size) {
            continue;
        }

        const scheduledCourses = option.map((course: string, index: number) => new CourseScheduled(course, index+1));
        const studyScheduleOption = new StudySchedule(studyDesiredId, scheduledCourses);

        result.push(studyScheduleOption);
    }
    
    return result;
}

const buildOptions = (availables: string[], courses: CourseDesired[]): string[][] => {
    const permutations = permutator(availables);
    const options: string[][] = [];

    let unlockeds = courses
        .filter((course: CourseDesired) => availables.includes(course.dependency))
        .map((course: CourseDesired) => course.name);
        
    if (unlockeds.length == 0) {
        permutations.forEach(option => options.push(option));

        return options;
    }

    const currentOptions = buildOptions(unlockeds, courses);
    
    permutations.forEach(option => {
        currentOptions.forEach(currentOption => {
            options.push([...option, ...currentOption]);
        });
    });

    return options;
}

export {
    buildStudyScheduleOptions
}