import { StudyDesired } from "@domain/study-desired";
import { create } from "@infrastructure/repository/study-desired";

const registStudyScheduleRequest = async(studyDesired: StudyDesired) => {
    await create(studyDesired);
}

export {
    registStudyScheduleRequest
}