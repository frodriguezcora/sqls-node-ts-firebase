namespace NodeJS {
    interface ProcessEnv {
        PORT: number
        NODE_ENV: string
        GOOGLE_APPLICATION_CREDENTIALS: string
    }
}