import Config from "./sql-server-credentials.json";
import { ConnectionPool } from "mssql";

var connection: any;

const initDbConnection = async () => {
    const pool = new ConnectionPool(Config);
    connection = await pool.connect();

    console.log("Data base conected.");
}

const getConnection = () => connection;

export {
    initDbConnection,
    getConnection
}