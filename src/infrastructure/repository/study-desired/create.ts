import { StudyDesired, StudyDesiredId } from "@domain/study-desired";
import { getConnection } from "@infrastructure/database";
import { DataBaseError } from "@infrastructure/database/data-base-error";
import { DesiredCourseTable } from "@infrastructure/tables/desired-course";
import { DesiredStudyTableName, DesiredStudyColumns } from "@infrastructure/tables/desired-study";
import { Request, VarChar, Transaction } from "mssql";

export default async (entity: StudyDesired) => {
    const connection = getConnection();
    const transaction = new Transaction(connection);
    
    try {
        await transaction.begin();
        const request = new Request(transaction);

        const result = await request
            .input('userUid', VarChar, entity.student.uid)
            .query(`INSERT INTO ${DesiredStudyTableName}
                    (${DesiredStudyColumns.USER_UID})
                    OUTPUT inserted.${DesiredStudyColumns.ID}
                    VALUES (@userUid);`);

        const id = result.recordset[0][DesiredStudyColumns.ID];

        const courseRequestTable = DesiredCourseTable();
        entity.courses.forEach((course) => 
            courseRequestTable.rows.add(
                id,
                course.name,
                course.dependency ?? ""));

        await request.bulk(courseRequestTable);

        await transaction.commit();
        entity.id = new StudyDesiredId(id);     
    }
    catch(error: any) {
        transaction.rollback();
        throw new DataBaseError("Error in Study Desired Creation", error);
    }
}