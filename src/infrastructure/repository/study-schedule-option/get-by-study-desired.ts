import { StudyDesiredId } from "@domain/study-desired";
import { getConnection } from "@infrastructure/database";
import { DataBaseError } from "@infrastructure/database/data-base-error";
import { ScheduledStudyOptionTableName, ScheduledStudyOptionTableColumns } from "@infrastructure/tables/scheduled-study-option";
import { ScheduledCourseTableName, ScheduledCourseTableColumns } from "@infrastructure/tables/scheduled-course";
import { Request } from "mssql";
import { CourseScheduled, StudySchedule, StudyScheduleId } from "@domain/study-schedule";

export default async (studyDesiredId: StudyDesiredId): Promise<StudySchedule[]> => {
    const connection = getConnection();
    const request = new Request(connection);
    
    try {
        const result = await request
            .query(`SELECT 
                        ${ScheduledCourseTableName}.*,
                        ${ScheduledStudyOptionTableColumns.DESIRED_STUDY_ID}

                    FROM ${ScheduledStudyOptionTableName}
                    LEFT JOIN ${ScheduledCourseTableName} 
                        ON ${ScheduledStudyOptionTableName}.${ScheduledStudyOptionTableColumns.ID} = ${ScheduledCourseTableColumns.SCHEDULED_STUDY_OPTION_ID}

                    WHERE ${ScheduledStudyOptionTableColumns.DESIRED_STUDY_ID} = ${studyDesiredId.value};`);

        const optionsId: number[] = Array.from(new Set(result.recordset.map(element => element[ScheduledCourseTableColumns.SCHEDULED_STUDY_OPTION_ID])));
        const response : StudySchedule[] = [];

        for (const optionId of optionsId) {
            const optionRows: any[] = result.recordset.filter(row => row[ScheduledCourseTableColumns.SCHEDULED_STUDY_OPTION_ID] == optionId);

            const courses: CourseScheduled[] = optionRows
            .map(element => new CourseScheduled(
                element[ScheduledCourseTableColumns.NAME],
                element[ScheduledCourseTableColumns.ORDER]
            ));

            response.push(new StudySchedule(
                new StudyDesiredId(optionRows[0][ScheduledStudyOptionTableColumns.DESIRED_STUDY_ID]),
                courses,
                new StudyScheduleId(optionId)));
        };

        return response;
    }
    catch(error: any) {
        throw new DataBaseError("Error in Get Study Schedule options", error);
    }
}