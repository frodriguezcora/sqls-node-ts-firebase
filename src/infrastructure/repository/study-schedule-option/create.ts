import { StudySchedule, StudyScheduleId } from "@domain/study-schedule";
import { getConnection } from "@infrastructure/database";
import { DataBaseError } from "@infrastructure/database/data-base-error";
import { ScheduledStudyOptionTableName, ScheduledStudyOptionTableColumns } from "@infrastructure/tables/scheduled-study-option";
import { ScheduledCourseTable } from "@infrastructure/tables/scheduled-course";
import { Request, Transaction, Int } from "mssql";

export default async (option: StudySchedule) => {
    const connection = getConnection();
    const transaction = new Transaction(connection);

    try {
        await transaction.begin();
        const request = new Request(transaction);

        const result = await request
            .input('scheduleRequestId', Int, option.studyDesiredId.value)
            .query(`INSERT INTO ${ScheduledStudyOptionTableName}
                    (${ScheduledStudyOptionTableColumns.DESIRED_STUDY_ID})
                    OUTPUT inserted.${ScheduledStudyOptionTableColumns.ID}
                    VALUES (@scheduleRequestId);`);

        const optionId = result.recordset[0][ScheduledStudyOptionTableColumns.ID];

        const scheduledCourseTable = ScheduledCourseTable();
        option.courses.forEach(course => 
            scheduledCourseTable.rows.add(
                optionId,
                course.name,
                course.order));

        await request.bulk(scheduledCourseTable);
        await transaction.commit();

        option.id = new StudyScheduleId(optionId);
    }
    catch(error: any) {
        transaction.rollback();
        throw new DataBaseError("Error in Study Schedule Options Creation", error);
    }
}