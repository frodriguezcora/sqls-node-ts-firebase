import { Table, Int } from "mssql";

const ScheduledStudyOptionTableName = "scheduled_study_option";

const ScheduledStudyOptionTableColumns = Object.freeze({ 
    ID :  "id", 
    DESIRED_STUDY_ID:  "desired_study_id",
});

const ScheduledStudyOptionTable = (): Table => {
    const table = new Table(ScheduledStudyOptionTableName);
    table.create = true;
    table.columns.add(ScheduledStudyOptionTableColumns.DESIRED_STUDY_ID, Int, { nullable: false, primary: false });

    return table;
}

export {
    ScheduledStudyOptionTable,
    ScheduledStudyOptionTableName,
    ScheduledStudyOptionTableColumns
}


