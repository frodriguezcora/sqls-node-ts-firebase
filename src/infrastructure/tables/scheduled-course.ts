import { Table, VarChar, Int } from "mssql";

const ScheduledCourseTableName = "scheduled_course";

const ScheduledCourseTableColumns = Object.freeze({ 
    ID :  "id", 
    SCHEDULED_STUDY_OPTION_ID:  "scheduled_study_option_id",
    NAME:  "name",
    ORDER:  "order"
});

const ScheduledCourseTable = (): Table => {
    const table = new Table(ScheduledCourseTableName);
    table.create = true;
    table.columns.add(ScheduledCourseTableColumns.SCHEDULED_STUDY_OPTION_ID, Int, { nullable: false, primary: false });
    table.columns.add(ScheduledCourseTableColumns.NAME, VarChar(100), { nullable: false, primary: false });
    table.columns.add(ScheduledCourseTableColumns.ORDER, Int, { nullable: false, primary: false });

    return table;
}

export {
    ScheduledCourseTable,
    ScheduledCourseTableName,
    ScheduledCourseTableColumns
}


