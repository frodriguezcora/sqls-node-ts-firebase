import { Table, VarChar, Int } from "mssql";

const DesiredCourseTableName = "desired_course";

const DesiredCourseTableColumns = Object.freeze({ 
    ID :  "id", 
    DESIRED_STUDY_ID:  "desired_study_id",
    NAME:  "name",
    REQUIRED:  "required"
});

const DesiredCourseTable = (): Table => {
    const table = new Table(DesiredCourseTableName);
    table.create = true;
    table.columns.add(DesiredCourseTableColumns.DESIRED_STUDY_ID, Int, { nullable: false, primary: false });
    table.columns.add(DesiredCourseTableColumns.NAME, VarChar(100), { nullable: false, primary: false });
    table.columns.add(DesiredCourseTableColumns.REQUIRED, VarChar(100), { nullable: false, primary: false });

    return table;
}

export {
    DesiredCourseTable
}


