import { Table, VarChar, Int } from "mssql";

const DesiredStudyTableName = "desired_study";

const DesiredStudyColumns = Object.freeze({ 
    ID :  "id", 
    USER_UID:  "user_uid" 
});

const DesiredStudyTable = (): Table => {
    const table = new Table(DesiredStudyTableName);
    table.create = true;
    table.columns.add(DesiredStudyColumns.USER_UID, VarChar(128), { nullable: false, primary: false });

    return table;
}

export {
    DesiredStudyTable,
    DesiredStudyTableName,
    DesiredStudyColumns
}

