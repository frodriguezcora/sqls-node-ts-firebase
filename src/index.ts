import "dotenv/config";
import "reflect-metadata";
import "./aliases";
import { initFirebaseAdmin, initFirebaseClient } from "@api/auth";
import api from "@api/server";
import { initDbConnection } from "@infrastructure/database";

initDbConnection();
initFirebaseAdmin();
initFirebaseClient();
api.listen(process.env.PORT);

console.log(`Listening on port ${process.env.PORT}`);
