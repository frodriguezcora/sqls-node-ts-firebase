# STUDY SCHEDULE SERVICE


## SPECIFICATION
Your task is to create a backend REST service that will receive the list of desired micro-courses in JSON format. The payload data is not organized in any specific order.
Please, develop a service implementation that can create a study schedule that lists courses in an order that respects the constraints.

### Technical requirements
1. The solution must be written in NodeJS or GoLang
2. The solution must use a SQL database
3. Since the solution requires a service like SQL server, this service should be provisioned using docker compose
4. The solution must include unit tests and e2e tests
5. The solution must have a README.md with the instructions on how to run the project
6. API endpoints should be protected with authentication using Firebase Authentication

### Input Example
```json
{
	"userId": “30ecc27b-9df7-4dd3-b52f-d001e79bd035”,
	"courses": [
		{
			"desiredCourse": "PortfolioConstruction",
			"requiredCourse": "PortfolioTheories"
		},
		{
			"desiredCourse": "InvestmentManagement",
			"requiredCourse": "Investment"
		},
		{
			"desiredCourse": "Investment",
			"requiredCourse": "Finance"
		},
		{
			"desiredCourse": "PortfolioTheories",
			"requiredCourse": "Investment"
		},
		{
			"desiredCourse": "InvestmentStyle",
			"requiredCourse": "InvestmentManagement"
		}
	]
}
```

### Output Example

|  Course | Order |
| ------------ | ------------ |
| Finance | 0 |
| Investment | 1 |
| InvestmentManagement | 2 |
| PortfolioTheories | 3 |
| InvestmentStyle | 4 |
| PortfolioConstruction | 5 |



## Environment configuration
You need rename (remove .dist) and set your values in 4 files
1. _src\api\auth\firebase-admin-credentials.json.dist_
2. _src\api\auth\firebase-client-credentials.json.dist_
3. _src\infrastructure\database\sql-server-credentials.json.dist_
4. _.env.dist_

## Run the application
```
npm run dev
```

## Test Endpoints
You can import the Postman Collection and Postman Environment in your postman account.
You can find the related files in _./postman_

I recommend use postman online version, only require install the postman agent.
https://www.postman.com/

## Database Files
##### It's important run the script to initialize the database schema.
1. Database script: _database\data-base-script.sql_
2. Database diagram: _database\data-base-diagram.jpg_


## Important
* To prevent a front application, I am using the firebase client in the backend.
* I prefer move the user endpoints to other service to wrap this endpoints in a service provider.
* I keep the user endpoints as public to easy the important endpoints.
* To use the private endpoints you need create a user, login with this user and include the token response in the postman environment key.
* The token may expire.
* I used typescript and ES6 to prevent type errors and enable use modern features of javascript.
* To define the folder structure, I aplied the pattern CQRS and DDD.
* To handle the table names and table column names, I created constants to detect errors in compilation time.

## Pending Feature
I had problems with docker-compose, sorry!
I am working this.

